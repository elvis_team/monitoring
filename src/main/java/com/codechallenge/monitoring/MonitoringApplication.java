package com.codechallenge.monitoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


// RABBIT_URI = amqp://localhost
@SpringBootApplication
//This service is has been moved to a container.
public class MonitoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringApplication.class, args);
	}
}
